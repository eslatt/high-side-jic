### Description

This pre-receive hook ensures that pushes are rejected unless each commit references at least 1 appropriate jira issue from the commit message.  The hook is installed at the GitLab instance level and configured at the project level with a json file stored in the project repository.

### Configuration

#### Name and Location

`.jira_issue_check.json`

located at the root of the git repository

#### Attributes

| name                     | type    | default                                             | description                                                  |
| ------------------------ | ------- | --------------------------------------------------- | ------------------------------------------------------------ |
| jira_issue_check_enabled | boolean | false                                               | controls whether the hook is applied                         |
| author_validator_enabled | boolean | false                                               | determines whether the git commiter email address must be the same as the jira issue assignee email address |
| jira_jql                 | string  | empty string                                        | allows projects to define which jira issues are appropriate for references |
| verbose                  | boolean | env var jic_verbose_default if it exists else false | enables extra stdout                                         |

#### Example

````
{
   "author_validator_enabled": false,
   "jira_issue_check_enabled": true,
   "jira_jql": "",
   "verbose": false
}
````

#### Notes

Invalid configuration json will result in the push being declined.

A single push can contain multiple commits.  When this happens, each commit must reference an appropriate jira issue as defined in the configuration file.  All commits of the push are evaluated against the configuration file that is found in the last commit.  This way a faulty configuration file can be fixed with an additional commit.  The *Test Cases* section below includes multiple cases of multiple-commit pushes.

A single commit can reference multiple jira issues.  When this happens, only one of the jira ids have to reference an appropriate jira issue.  The *Test Cases* section below includes multiple cases of multiple-jira-references commits.

### Installation

`jira_issue_check.py` is put in the default location for pre-receive git hooks and made executable

#### Jira Location and Credentials

The `jira_email_from_jql` function must be modified to use the appropriate jira url and credentials.  **Read-only access for the jira service acccount is preferred.**

Included `jira_access_test.sh` can be used to help test jira access as shown below.

```bash
#!/bin/bash

jira_url=edjira.ddns.net
jira_user=GitLab.tests
jira_password=dummy

curl --request GET \
  --url "https://${jira_url}/rest/api/2/search?maxResults=1&jql=id%3DJIC-2&fields=assignee" \
  --header "Authorization: Basic `echo -n "$jira_user:$jira_password" | base64`" \
  --header 'Content-Type: application/json'
```

stdout out of successful test

```
{"expand":"names,schema","startAt":0,"maxResults":1,"total":1,"issues":[{"expand":"operations,editmeta,changelog,transitions,renderedFields","id":"27922","self":"https://edjira.ddns.net/rest/api/2/issue/27922","key":"JIC-2","fields":{"assignee":null}}]}%  
```

### Test Environment

A dummy jira project and issues have been created in a test jira deployment.  Further more, a series of commits have been contrived in the `test-commits` branch of this git repository.  These are described in *Jira* and *Commits* sections below.  Their usage is described in the *Test Cases* section below.

#### Jira

JIC-1 handle the lamb case (assigned to esg)

JIC-2 handle the lion case (not assigned to esg)

#### Commits

the following commits are used to support the regression test

they are created in the test-commits branch pretty close to the original trunk

this way, an regression test shell can invoke the python in a way that is pretty close to the target environment

| commit hash | commit message                                               | passes |
| ----------- | ------------------------------------------------------------ | ------ |
| ea51a95     | starts rt                                                    | 1      |
| f3deffd     | no jira id is given; no .jira_issue_check.json file exists   | 1      |
| 82e7d41     | no jira id is given; jira_issue_check_enabled true           | 0      |
| 7850ff5     | JIC-22132 does not exist; jira_issue_check_enabled true      | 0      |
| 19fa4ea     | JIC-22132 does not exist; jira_issue_check_enabled false     | 1      |
| 83f7ecf     | JIC-1 includes lamb work; author_validator_enabled false     | 1      |
| db20c3c     | JIC-2 includes lion work; author_validator_enabled false     | 1      |
| 0b84436     | JIC-1 includes lamb work; author_validator_enabled true      | 1      |
| 6413cbb     | JIC-2 includes lion work; author_validator_enabled true      | 0      |
| 062b2b1     | JIC-1; jira_jql is junk                                      | 0      |
| d5562e1     | JIC-1 includes lamb work; jira_jql gives that summary must include the word lamb | 1      |
| 8fd675a     | JIC-2 includes lamb work; jira_jql gives that summary must include the word lamb | 0      |
| bc1358a     | JIC-22132 JIC-22133                                          | 0      |
| 4600bae     | JIC-22132 JIC-1                                              | 1      |
| 1423179     | JIC-1 JIC-22133                                              | 1      |
| 9c73208     | JIC-1 JIC-2                                                  | 1      |
| f2b2b56     | includes broken configuration json                           | 0      |

### Test Cases

The following test cases are applied to jira_issue_check.py by running *rt.sh* from *rt*.  This scipt invokes the hook using the same stdin interface used by git on the server.  The return code, normal stdout and verbose stdout are captured in `.returncode`, `.not-verbose.stdout` and `verbose.stdout` files for each test case in the *audit* directory.  This directory is then compared to the validated results from the *control* directory.  In this way, changes to the code can be tested against all of the cases within a minute.

#### todo

* add test cases to cover the ignore clause related to references with 0{40}
* add test cases to cover multiline githook stdin

#### trivial cases

0 trivial non-configured case - push succeeds

* f3deffd

1 enabled jic - no jira-id - push fails

* 82e7d41

2 enabled jic - faulty jira-issue-id fails - push fails

* 7850ff5

3 disabled jic - faulty jira-issue-id - push succeeds

* 19fa4ea

#### author_validator_enabled flag

4 disable author_validator_enabled for JIC-1 - push succeeds

* 83f7ecf

5 disable author_validator_enabled for JIC-2 - push succeeds

* db20c3c

6 enable author_validator_enabled for JIC-1 - push succeeds

* 0b84436

7 enable author_validator_enabled for JIC-2 - push fails

* 6413cbb

#### jira_jql (author_validator_enabled is disabled)

8 junk jira_jql - push fails

* 062b2b1

9 passes additional issue scrutiny in jira_jql - push succeeds

* d5562e1

10 fails additional issue scrutiny in jira_jql - push fails

* 8fd675a

#### multiple jira ids (author_validator_enabled is disabled, jira_jql is empty)

11 fail fail - push fails

* bc1358a

12 fail pass - push succeeds

* 4600bae

13 pass fail - push succeeds

* 1423179

14 pass pass - push succeeds

* 9c73208

#### multiple jira commits

15 mjc - fail fail - push fails

* 82e7d41
* 7850ff5

16 mjc - fail pass - push fails

* bc1358a
* 4600bae

17 mjc - pass fail - push fails

* d5562e1
* 8fd675a

18 mjc - pass pass - push succeeds

* 4600bae
* 1423179

19 mjc - commit order - push succeeds

* 4600bae
* 1423179
* 9c73208

#### miscellaneous

20 broken config json - push fails

* f2b2b56

