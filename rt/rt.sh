test_case () {
  skip_flag=$2
  if [ "$skip_flag" == "skip" ]; then
    echo skipping test $3
    cp "./control/$3.stdout" ./audit
  else
    test_stdin=$1
    test_description=$3
    echo test case $test_description
    export jic_verbose_default=True
    echo $test_stdin | python3 ../jira_issue_check.py > "./audit/$3.verbose.stdout"
    export jic_verbose_default=False
    echo $test_stdin | python3 ../jira_issue_check.py > "./audit/$3.not-verbose.stdout"
    test_returncode=$?
    echo "[return code $test_returncode]" >"./audit/$3.returncode"
  fi
}

rm -rf ./audit
mkdir ./audit

test_case 'ea51a95 f3deffd test-commits'  nskip '0 trivial non-configured case - push succeeds' 
test_case 'f3deffd 82e7d41 test-commits'  nskip '1 enabled jic - no jira-id - push fails' 
test_case '82e7d41 7850ff5 test-commits'  nskip '2 enabled jic - faulty jira-issue-id fails - push fails' 
test_case '7850ff5 19fa4ea test-commits'  nskip '3 disabled jic - faulty jira-issue-id - push succeeds' 
test_case '19fa4ea 83f7ecf test-commits'  nskip '4 disable author_validator_enabled for JIC-1 - push succeeds' 
test_case '83f7ecf db20c3c test-commits'  nskip '5 disable author_validator_enabled for JIC-2 - push succeeds' 
test_case 'db20c3c 0b84436 test-commits'  nskip '6 enable author_validator_enabled for JIC-1 - push succeeds' 
test_case '0b84436 6413cbb test-commits'  nskip '7 enable author_validator_enabled for JIC-2 - push fails' 
test_case '6413cbb 062b2b1 test-commits'  nskip '8 junk jira_jql - push fails' 
test_case '062b2b1 d5562e1 test-commits'  nskip '9 passes additional issue scrutiny in jira_jql - push succeeds' 
test_case 'd5562e1 8fd675a test-commits'  nskip '10 fails additional issue scrutiny in jira_jql - push fails' 

test_case '8fd675a bc1358a test-commits'  nskip '11 fail fail - push fails' 
test_case 'bc1358a 4600bae test-commits'  nskip '12 fail pass - push succeeds' 
test_case '4600bae 1423179 test-commits'  nskip '13 pass fail - push succeeds' 
test_case '1423179 9c73208 test-commits'  nskip '14 pass pass - push succeeds' 

test_case 'f3deffd 7850ff5 test-commits'  nskip '15 mjc - fail fail - push fails' 
test_case '8fd675a 4600bae test-commits'  nskip '16 mjc - fail pass - push fails' 
test_case '062b2b1 8fd675a test-commits'  nskip '17 mjc - pass fail - push fails' 
test_case 'bc1358a 1423179 test-commits'  nskip '18 mjc - pass pass - push succeeds' 
test_case 'bc1358a 9c73208 test-commits'  nskip '19 mjc - commit order - push succeeds' 

test_case '9c73208 f2b2b56 test-commits'  nskip '20 broken config json - push fails' 

diff -r audit control
if [ "$?" == 0 ]
then
  echo "audit and control are identical"
  exit 0
else
  echo "audit and control are different"
  diff -qr audit control
  exit 1
fi